using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int vida;
    public int maxVida;
    //public TextMeshProUGUI textVida;
    private Vector2 checkpoint;
    
    void Start()
    {
        checkpoint =  transform.position;
        vida = maxVida;
    }
    void Update()
    {
        //textVida.text = vida.ToString();
    }
    void Respawn()
    {
        transform.position = checkpoint;
        vida = maxVida;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //se colide com layer dano perde vida
        if(collision.gameObject.layer == LayerMask.NameToLayer("dano"))
        {
           vida -= collision.gameObject.GetComponent<EnemyTest>().dano;

            if(vida <= 0)
            {
            Respawn();
            } 
        }
    }
}
