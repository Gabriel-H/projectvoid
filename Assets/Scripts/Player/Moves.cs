using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moves : MonoBehaviour
{
    public float velocidade;
    private Rigidbody2D rb;
    public float inputx;
    public float pulo;
    public bool podepular;
    public float inputDirection;
    public bool isDirectionRight;
    public Animator anim;

    void Start()
    {
       rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        Getinput();
        Flip();
        animController();
    }
    void FixedUpdate()
    {
        MoveLogic();
    }
    void Getinput()
    {
        //Pega os input
        inputx = Input.GetAxis("Horizontal");
        if(Input.GetKeyDown(KeyCode.Space) && podepular == true){
            rb.velocity = new Vector2(rb.velocity.x, pulo);
        }
    }
    void MoveLogic()
    {
        //faz andar
        rb.velocity = new Vector2(inputx * velocidade, rb.velocity.y);
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        //faz pular so em contato com layer ground
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground")) {
            podepular = true;
        }
    }
     void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground")) {
            podepular = false;
        }
    }
    void Flip()
    {
        //faz o boneco virar 
        if(isDirectionRight && inputx > 0)
        {
            FlipLogic();
        }
        if(!isDirectionRight && inputx < 0)
        {
            FlipLogic();
        }
    }
    void FlipLogic()
    {
        isDirectionRight = !isDirectionRight;
        transform.Rotate(0f, 180.0f, 0f);
    }
    void animController()
    {
        //animacoes idle, run e jump/fall
        anim.SetFloat ("Horizontal", rb.velocity.x);
        anim.SetFloat ("Vertical", rb.velocity.y);
        anim.SetBool ("Podepular", podepular);
    }
}