using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{

    public Animator anim;

    public Transform attackPoint;
    public LayerMask enemyLayers;

    public float attackRange;
    public int attackDamage;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            Attack();
        }
    }

    void Attack()
    {
        //Rodar a animacao
        anim.SetTrigger("Attack");

        //detectar inimigos no range do ataque
        Collider2D [] hitEnemies =  Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        //danar eles
        foreach (Collider2D enemy in hitEnemies) {
            enemy.GetComponent<EnemyTest>().TakeDamage(attackDamage);
        }
    }

    //faz aparecer o range do amigao
    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}
