using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTest : MonoBehaviour
{
    private Rigidbody2D rb;

    public int maxVida = 10;
    int vida;
    public int dano;

    void Start()
    {
        vida = maxVida;
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {

    }

    public void TakeDamage(int danoTomado)
    {
        vida -= danoTomado;

        // rodar a animacao de "ai doeu"
        if(vida <= 0){
            Die();
        }
    }

    void Die()
    {
        Debug.Log("morreu ai irmao");

        //animacao de morreu

        //desabilitar o inimigo
    }
}
