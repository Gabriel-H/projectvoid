using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{

    [Header("MoveBase")]
    [SerializeField] private Transform[] targetPos;
    private int idPos;
    [SerializeField] private float speed;

    [Header("Fov")]
    [SerializeField] private float fovAngle;
    [SerializeField] private Transform fovPoint;
    [SerializeField] private float range;

    [Header("Life")]
    [SerializeField] private int maxLife;
    private int life;
    private bool dead;

    private bool isRight;

    private GameObject player;
    private bool inVision;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        player = GameObject.FindWithTag("Player");

        life = maxLife;

        transform.position = targetPos[0].position;
        idPos = 1;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        FlipEnemy();
        DeadLogic();
    }

    protected void EnemyMove()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPos[idPos].position, speed * Time.deltaTime);

        if (transform.position == targetPos[idPos].position)
            idPos += 1;

        if (idPos == targetPos.Length)
            idPos = 0;
    }

    protected void RayMove()
    {
        Vector2 direcao = player.transform.position - transform.position;
        float angle = Vector3.Angle(direcao, fovPoint.right);

        RaycastHit2D hit = Physics2D.Raycast(fovPoint.position, direcao, range);

        if (angle < fovAngle / 2 && hit.collider != null && hit.collider.CompareTag("Player"))
        {
            Debug.DrawRay(fovPoint.position, direcao, Color.blue);
            inVision = true;
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        }
        else
        {
            EnemyMove();
            inVision = false;
        }

    }
    protected void FlipEnemy()
    {
        if (isRight && transform.position.x <= targetPos[idPos].position.x && inVision == false)
        {
            FlipLogic();
        }
        if (!isRight && transform.position.x >= targetPos[idPos].position.x && inVision == false)
        {
            FlipLogic();
        }
    }

    private void FlipLogic()
    {
        isRight = !isRight;
        transform.Rotate(-180f, 0f, -180f);
    }

    private void DeadLogic()
    {
        if (life <= 0)
        {
            dead = true;
        }

        if (dead == true)
        {
            Destroy(gameObject);
        }
    }

}
